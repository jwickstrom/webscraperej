const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const pageURL = 'https://en.wikipedia.org/wiki/List_of_wars_involving_Sweden';
  try {
    // try to go to URL
    await page.goto(pageURL);
    console.log(`opened the page: ${pageURL}`);
  } catch (error) {
    console.log(`failed to open the page: ${pageURL} with the error: ${error}`);
  }

  //finding the h1 elements
  const elementSelector = 'h1';
  await page.waitForSelector(elementSelector, { timeout: 0 });
  const postH1 = await page.$$eval(elementSelector, postLinks => postLinks.map(Text => Text.textContent));
  // console.log(postH1);

  // get the title of the site
  const titleSelector = '#firstHeading';
  await page.waitForSelector(titleSelector);
  const pageTitle = await page.$eval(titleSelector, titleSelector => titleSelector.innerHTML);
  // console.log(pageTitle);
  

  // get the inledning of the site
  const pSelector = '.mw-parser-output > p:nth-child(2)';
  await page.waitForSelector(pSelector);
  const pageP = await page.$eval(pSelector, testSelector => testSelector.innerHTML);
  console.log(pageP);

  
  
  // .wikitable > tbody:nth-child(1) > tr:nth-child(1) > th:nth-child(1)
  // .wikitable > tbody:nth-child(1) > tr:nth-child(3)
  // .wikitable > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(1)
  // .wikitable > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2)
  // .wikitable > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(3)
  // .wikitable > tbody:nth-child(1) > tr:nth-child(4)
  // .wikitable > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(1)

  await browser.close();
})();

function formatText(inputString) {
  while((inputString.replace(/(<a>[a-z-A-Z-0-9-\s]+<\/a>)/, '') !== inputString)) {
    inputString = inputString.replace(/(<a>[a-z-A-Z-0-9-\s]+<\/a>)/, '')
  }
}