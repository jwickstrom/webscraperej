getData();



function getData() {
    const slider = document.getElementById("timeSlider");
    const infoOutput = document.getElementById("idOutput");
    const yearOutput = document.getElementById("yearOutput");
    const conclusionOutput = document.getElementById("conclusionOutput");

    fetch("/data")
        .then((rawDataObject) => rawDataObject.json())
        .then((warArray) => {
            
            slider.max = warArray.length - 1;
            
            // Display the default values
            // infoOutput.innerHTML = slider.value; 
            yearOutput.innerHTML = "Year: " + warArray[1].year;
            warOutput.innerHTML = "War: " + warArray[1].war;
            conclusionOutput.innerHTML = "Conclusion: " + warArray[1].conclusion;


            // Update the current slider value (each time you drag the slider handle)
            slider.oninput = function () {
                // idOutput.innerHTML = this.value;
                yearOutput.innerHTML = warArray[this.value].year;
                warOutput.innerHTML = warArray[this.value].war;
                conclusionOutput.innerHTML = warArray[this.value].conclusion;
            }
        });
    // return warArray;
}


