const puppeteer = require('puppeteer');
const fs = require("fs");
const { xml } = require('cheerio');

//From https://justmarkup.com/articles/2019-01-04-using-puppeteer-to-crawl-pages-and-save-them-as-markdown-files/
(async() => {
    // start the browser, Awaiting a promise to be fulfilled
    const browser = await puppeteer.launch();
    // open a new page
    const page = await browser.newPage();
    const pageURL = 'https://en.wikipedia.org/wiki/List_of_wars_involving_Sweden';
    try {
        // try to go to URL
        await page.goto(pageURL);
        console.log(`opened the page: ${pageURL}`);
    } catch (error) {
        console.log(`failed to open the page: ${pageURL} with the error: ${error}`);
    }
    // wait a bit so that the browser finishes executing JavaScript
	//await page.waitFor(1 * 1000);
        
    const html = await page.content();
	
    // Find the elements with selector p

    //finding the h1 elements
    const postsSelector = '.wikitable > * > tr ';
    await page.waitForSelector(postsSelector, { timeout: 0 });
    const post1 = await page.$$eval(postsSelector, postLinks => postLinks.map(Text => Text.textContent));
    //console.log(post1)

    let arrayen = []
    let id = 1
    for (let i = 0; i < post1.length; i++) {
     
        let str = post1[i].split("\n\n")
        console.log("STR" + typeof str)
        
        let first = str.slice(0,1)        
        let second = str.slice(1,2)
        let third = str.slice(2,3)

        //Current output. There is an extra \n at the beginning and the end. 
          /*{
            "\nid": 3,
            "Year": "1015–1018",
            "War": "Norwegian–Swedish War",
            "Conclustion": "Treaty of Kungahälla (1020)\n"
          }*/
          
        //To erase the \ns, we slice the first and the third element
        first = str.map(first => first.slice(1))
        third = third.map(third => third.substring(0,third.length - 1))

        const obj = {"id": id, "year": first[0], "war": second[0], "conclusion": third[0]}
        
        if(second[0] === undefined) {
            console.log("Wronk, skip!")
        }else{
        arrayen.push(obj)

        console.log(obj)
        id++
            
        }

    }
    //Stringify the array object into string in json format
    var new_json = JSON.stringify(arrayen, null, 2)

    //Putting the output file in /data folder
    fs.writeFileSync(__dirname + '/../data/data.json', new_json);

    //Write to the file

    //Console it for a feeling of control
    //console.log(new_json)
    console.log("done")    

   // console.log(post1)
    //console.log("--------------------------"+typeof post1)
    //console.log(post1.sp)
    // all done, close the browser
    await browser.close();
    process.exit()
})();

